
#include  <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>


struct image *transform(struct image *image, struct image * (transformation_func)(struct image * old_image, struct image * new_image),bool reversed);
struct image *turn_left(struct image * old_image, struct image * new_image);


