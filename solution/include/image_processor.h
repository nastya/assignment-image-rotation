#include  <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include "IO.h"


static const uint16_t BF_TYPE = 0x4D42;
static const uint32_t BF_RESERVED = 0;
static const uint32_t BI_SIZE = 40;
static const uint16_t BI_PLANES = 0;
static const uint16_t BIT_COUNT = 24;
static const uint32_t BI_COMPRESSION = 0;
static const uint32_t BI_X_PELS_PER_METER = 0;
static const uint32_t BI_Y_PELS_PER_METER = 0;
static const uint32_t BI_CLR_USED = 0;
static const uint32_t BI_CLR_IMPORTANT = 0;



struct __attribute__ ((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

enum status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    WRITE_OK = 0,
    WRITE_ERROR
};


enum status to_bmp( FILE* out, struct image * image );
enum status from_bmp(FILE* input, struct image * image);
uint64_t padding(const struct image * img);
int image_destroy(struct image * img);
struct image *from_interlayer(FILE * f, char* argv, enum status (func)( FILE* out, struct image * image ),bool (file_func)(const char *fname, FILE **file));
int to_interlayer(FILE * f,struct image * result, char* argv, enum status (func)( FILE* out, struct image * image ),bool (file_func)(const char *fname, FILE **file)) ;
struct image * image_create(struct image * image, uint64_t width, uint64_t height);
struct image * image_create_malloc(uint64_t width, uint64_t height);
