#include <stdbool.h>
#include  <stdint.h>
#include <stdio.h>
#include <stdlib.h>
bool read_file(const char *fname, FILE ** file);
bool write_to_file(const char *fname, FILE ** file);
bool close_file(FILE* fname);

