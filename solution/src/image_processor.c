#include "image_processor.h"


struct bmp_header new_header(const struct image* img) {
    struct bmp_header bmp_header = {0};
    bmp_header.bfType =BF_TYPE;
    bmp_header.bfileSize = img->height * img->width * sizeof(struct pixel) +
            img->height * padding(img) * sizeof(struct pixel);
    bmp_header.bfReserved= BF_RESERVED;
    bmp_header.bOffBits = sizeof(struct bmp_header);
    bmp_header.biSize = BI_SIZE;
    bmp_header.biWidth = img->width;
    bmp_header.biHeight = img->height;
    bmp_header.biPlanes = BI_PLANES;
    bmp_header.biBitCount = BIT_COUNT;
    bmp_header.biCompression = BI_COMPRESSION;
    bmp_header.biSizeImage =  img->height * img->width * sizeof(struct pixel) + padding(img) * img->height;
    bmp_header.biXPelsPerMeter = BI_Y_PELS_PER_METER;
    bmp_header.biYPelsPerMeter = BI_X_PELS_PER_METER;
    bmp_header.biClrUsed = BI_CLR_USED;
    bmp_header.biClrImportant = BI_CLR_IMPORTANT;
    return bmp_header;
}

enum status from_bmp(FILE* input, struct image * image) {
    printf("FILE READING PROCESS HAS BEGAN\n");
    //struct image * image = malloc(sizeof(struct image));
    if(!input)return READ_INVALID_HEADER;
    struct bmp_header _new_header = new_header(image);
    if (fread(&_new_header, sizeof(struct bmp_header), 1, input) == 1){
        image = image_create(image, _new_header.biWidth, _new_header.biHeight);
        for (size_t i = 0; i < image->height; i++) {
            if(!fread(&(image->data[i * image->width]), sizeof(struct pixel), image->width, input)){free(image->data); return READ_INVALID_BITS;}
            if(fseek(input, (uint8_t) padding(image), SEEK_CUR)!=0){free(image->data); return READ_INVALID_SIGNATURE;}
        }

        //free(new_header);
        return READ_OK;
    } else {
        //free(new_header);
        return READ_INVALID_HEADER;
    }
}

enum status to_bmp(FILE* out, struct image * image) {
    struct bmp_header _new_header = new_header(image);
    if (fwrite(&_new_header, sizeof(struct bmp_header), 1, out) == 1) {
        const size_t fill = 0;
        for (uint32_t i = 0; i < image->height; i++) {
            if(!fwrite(&(image->data[i * image->width]), sizeof(struct pixel), image->width, out)){return WRITE_ERROR;}
            if(!fwrite(&fill, 1, image->width % 4, out)){return WRITE_ERROR;}
        }
        //free(_new_header);
        return WRITE_OK;
    }
    else {
        //free(_new_header);
        return WRITE_ERROR;
    }
}

uint64_t padding(const struct image * img){
    return img->width % 4;
}

int image_destroy(struct image * img){
    if (img->data)free(img->data);
    free(img);
    return 0;
}

struct image *from_interlayer(FILE * f, char* argv, enum status (func)( FILE* out, struct image * image ),bool (file_func)(const char *fname, FILE **file)){
    //printf("here1");
    if(!file_func(argv, &f)){
        fprintf(stderr,"UNABLE TO OPEN FILE");
        return NULL;
    }

    struct image * image = malloc(sizeof(struct image));
    if (func(f, image) != 0){
        fprintf(stderr,"FILE IS CORRUPTED\n");
        return NULL;
    }
    //printf("%"PRId8, image->data[0].g);
    if(!close_file(f)){
        fprintf(stderr,"UNABLE TO CLOSE FILE1");
        return NULL;
    }
    printf("READING SUCCESSFUL\n");
    return image;
}

int to_interlayer(FILE * f,struct image * result, char* argv, enum status (func)( FILE* out, struct image * image ),bool (file_func)(const char *fname, FILE **file)) {
    if (!file_func(argv, &f)) {
        fprintf(stderr,"UNABLE TO OPEN FILE");
        return 1;
    }
    if (func(f, result) != 0) {
        fprintf(stderr,"AN ERROR OCCURRED DURING FILE WRITING");
        return 1;
    }
    if(!close_file(f)){
        fprintf(stderr,"UNABLE TO CLOSE FILE1");
        return 1;
    }
    return 0;
}

struct image * image_create_malloc(uint64_t width, uint64_t height){
    struct image * image = malloc(sizeof (struct image));
    image->width = width;
    image->height = height;
    image->data = malloc(image->height * image->width * sizeof(struct pixel));
    //printf("%"PRId64, image->width);
    //printf("\n%"PRId64, image->height);
    return image;
}

struct image * image_create(struct image * image, uint64_t width, uint64_t height){
    image->width = width;
    image->height = height;
    image->data = malloc(image->height * image->width * sizeof(struct pixel));
    //printf("%"PRId64, image->width);
    //printf("\n%"PRId64, image->height);
    return image;
}
