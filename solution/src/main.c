
#include "image_processor.h"
#include "spinner.h"




int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc == 3) {
        FILE * file_old = {0};

        struct image * image = from_interlayer(file_old, argv[1], from_bmp, read_file);
        printf("%"PRId64, image->width);
        struct image * result = transform(image,turn_left, true);
        printf("THE IMAGE IS ROTATED\n");
        FILE * file_new = {0};
        to_interlayer(file_new,result, argv[2],  to_bmp, write_to_file);
        image_destroy(image);
        image_destroy(result);
        printf("SUCCESS\n");
        return 0;
    }
    else {
        printf("COMMAND LINE ARGUMENTS ARE INVALID\n");
        return 1;
    }
}

