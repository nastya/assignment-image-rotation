#include "image_processor.h"
#include "spinner.h"


struct pixel get_pixel(const struct image * image, size_t x, size_t y) {
    const size_t pos = y * image->width + x;
    return image->data[pos];
}

struct pixel * get_pixel_p(struct image * image, size_t x, size_t y) {
    const size_t pos = y * image->width + x;
    return image->data + pos;
}


struct image *transform(struct image *image, struct image * (transformation_func)(struct image * old_image, struct image * new_image), bool reversed) {
    //printf("checkkkkkkk");
    if(!image)return NULL;
    if(reversed) {
        //printf("%"PRId64, image->width);
        //printf("\n%"PRId64, image->height);
        struct image * new_image = image_create_malloc(image->height, image->width);
        return transformation_func(image, new_image);
    }
    else {
        struct image *new_image = image_create_malloc(image->width, image->height);
        return transformation_func(image, new_image);
    }

}

struct image * turn_left(struct image * old_image, struct image * new_image) {

    for (uint64_t i = 0; i < old_image->width; i++) {
        for (uint64_t j = 0; j < old_image->height; j++) {

            *get_pixel_p(new_image, j, i) = get_pixel(old_image, i, new_image->width - j-1);
        }
    }

            return new_image;
}


