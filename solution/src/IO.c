#include "IO.h"


bool read_file(const char *fname, FILE **file){
    if((*file = fopen(fname, "rb")) != NULL)return true;
    return false;
}

bool write_to_file(const char *fname, FILE **file){
    if((*file = fopen(fname, "wb")) != NULL)return true;
    return false;
}

bool close_file(FILE* fname){
    if((EOF != fclose(fname)))return true;
    return false;
}

